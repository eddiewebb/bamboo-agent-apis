package ut.com.edwardawebb.bamboo.agentapis.rest;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentState;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.GenericEntity;

public class RemoteAgentStatusTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {
       
        assertEquals("wrong message","Hello World","Hello World");
    }
}
