package com.edwardawebb.bamboo.agentapis.tasks;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.struts.TextProvider;
import com.atlassian.util.concurrent.NotNull;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import javax.annotation.Nullable;
import java.util.Map;

public class UpdateRulesTaskConfigurator extends AbstractTaskConfigurator{
    private static final Logger LOG = Logger.getLogger(UpdateRulesTaskConfigurator.class);

    private final TextProvider textProvider;
    private final BambooPermissionManager permissionManager;
    private final BambooAuthenticationContext authenticationContext;

    public UpdateRulesTaskConfigurator(TextProvider textProvider, BambooPermissionManager permissionManager, BambooAuthenticationContext authenticationContext) {
        this.textProvider = textProvider;
        this.permissionManager = permissionManager;
        this.authenticationContext = authenticationContext;
    }

    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
        context.put("adminStatus",adminStatus());
    }


    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        context.put("adminStatus",adminStatus());
    }
    private String adminStatus() {
        final String username = authenticationContext.getUserName();
        if (!permissionManager.isAdmin(username)) {
             return "You are not an admin! You will not be able to add/save this task.";
        } else {
            return "Admin Status: pass";
        }
    }

    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        LOG.warn("checking perimissons");
        super.validate(params, errorCollection);
        final String username = authenticationContext.getUserName();

        if ( ! permissionManager.isAdmin(username))
        {
            errorCollection.addError("adminStatus", textProvider.getText("agent-apis.task.error.permission"));
        }
    }

}
