package com.edwardawebb.bamboo.agentapis.rest.admin;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.user.UserManager;
import com.edwardawebb.bamboo.agentapis.ao.AccessTokenService;
import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.UpgradePermissionModel;
import com.edwardawebb.bamboo.agentapis.services.ChaperoneService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.List;
import java.util.UUID;

/**
 * API used by agent-apis plugin.
 * Exposes any outstanding task, and enables forced completion ("delete")
 */
@Path("/tasks")
@PublicApi
public class TaskEndpoint {

    private ChaperoneService chaperoneService;
    private AccessTokenService accessTokenService;
    private final UserManager userManager;

    public TaskEndpoint(ChaperoneService chaperoneService, AccessTokenService accessTokenService, UserManager userManager) {
        this.chaperoneService = chaperoneService;
        this.accessTokenService = accessTokenService;
        this.userManager = userManager;
    }

    @GET
    @Path("/")
    public Response openTasks(){
        if(!isAdmin()){
            return Response.status(Status.FORBIDDEN).build();
        }
        List<TaskModel> tasks = chaperoneService.listAllOpenTasks();
        return Response.ok(tasks).build();
    }

    @DELETE
    @Path("{taskId}")
    public Response abort(@PathParam("taskId") int taskId)
    {
        if(!isAdmin()){
            return Response.status(Status.FORBIDDEN).build();
        }
        chaperoneService.abortTask(taskId);
        return Response.ok().build();
    }

    private boolean isAdmin() {
        String username = userManager.getRemoteUsername();
        if ( userManager.isAdmin(username) || userManager.isSystemAdmin(username) ){
            return true;
        }else{
            return false;
        }
    }
}