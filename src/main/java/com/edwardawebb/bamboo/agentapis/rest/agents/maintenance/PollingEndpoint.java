package com.edwardawebb.bamboo.agentapis.rest.agents.maintenance;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.edwardawebb.bamboo.agentapis.ao.AccessTokenService;
import com.edwardawebb.bamboo.agentapis.ao.model.AccessToken;
import com.edwardawebb.bamboo.agentapis.enums.AgentActivity;
import com.edwardawebb.bamboo.agentapis.services.ChaperoneService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.UUID;

/**
 * A resource of message.
 */
@Path("/polling")
@PublicApi
public class PollingEndpoint {

    private ChaperoneService chaperoneService;
    private AccessTokenService accessTokenService;


    public PollingEndpoint(ChaperoneService chaperoneService, AccessTokenService accessTokenService) {
        this.chaperoneService = chaperoneService;
        this.accessTokenService = accessTokenService;
    }


    @GET
    @Path("/version")
    @AnonymousAllowed
    public Response polling(@QueryParam("uuid") UUID uuid)
    {
        if(null == uuid){
            return   Response.status(Status.BAD_REQUEST).entity("No UUID Supplied").build();
        }
        AccessToken token = accessTokenService.findTokenByUuid(uuid);
        if(null != token && token.isAllowedToRead()){
            VersionModel response = chaperoneService.pollVersion();
            return Response.ok(response.asText()).build();
        }else{
            return Response.status(Status.FORBIDDEN).build();
        }

    }

}