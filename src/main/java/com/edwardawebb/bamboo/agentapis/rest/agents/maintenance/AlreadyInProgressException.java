package com.edwardawebb.bamboo.agentapis.rest.agents.maintenance;

@SuppressWarnings("serial")
public class AlreadyInProgressException extends RuntimeException {

    private int taskId;
    public AlreadyInProgressException(String message, int id) {
        super(message);
        this.taskId = id;
    }

    public int getTaskId(){
        return taskId;
    }
}
