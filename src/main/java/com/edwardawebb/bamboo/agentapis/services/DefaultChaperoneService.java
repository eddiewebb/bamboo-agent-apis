package com.edwardawebb.bamboo.agentapis.services;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.edwardawebb.bamboo.agentapis.rest.admin.TaskModel;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.VersionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.edwardawebb.bamboo.agentapis.ao.AgentInProgressService;
import com.edwardawebb.bamboo.agentapis.ao.model.AgentInProgress;
import com.edwardawebb.bamboo.agentapis.enums.AgentActivity;
import com.edwardawebb.bamboo.agentapis.enums.PermissionEnum;
import com.edwardawebb.bamboo.agentapis.rest.admin.AdminConfigurationResource;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.AlreadyInProgressException;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.UpgradePermissionModel;
import com.edwardawebb.bamboo.agentapis.rest.agents.state.AgentStateModel;

public class DefaultChaperoneService implements ChaperoneService {
    private final AgentService remoteAgentService;
    private  final AgentInProgressService agentInProgressService;
    private static final Logger log = LoggerFactory
            .getLogger(DefaultChaperoneService.class);
    private final ConfigurationService configurationService;


    public DefaultChaperoneService(AgentService remoteAgentService, AgentInProgressService agentInProgressService, ConfigurationService configurationService) {
        this.remoteAgentService = remoteAgentService;
        this.agentInProgressService = agentInProgressService;
        this.configurationService = configurationService;
    }
    
   

    @Override
    public UpgradePermissionModel markInProgressIfAllowed(AgentActivity upgrade, long agentId, UUID uuid) {
        log.info("Agent {} requesting update", agentId);
        AgentStateModel agentState = remoteAgentService.getStateFor(agentId);
        BuildAgent agent = remoteAgentService.getAgentManager().getAgent(agentId);
        AdminConfigurationResource config = configurationService.getActiveConfig();
        
        if(config.getStance().isAllowed()){
            List<AgentInProgress> currentActivity = agentInProgressService.existingActivityForAllAgents();
            log.debug("found {} other updates in progress",currentActivity.size());
            if(currentActivity.size() < config.getMaxactive()){    
                log.info("allowing {} to upgrade. They willbe disabled. The agent should respect any running jobs before it begins work.",agentId);
                try{
                    AgentInProgress task = agentInProgressService.markAgentInProgress(agent, AgentActivity.UPGRADE, uuid, agentState);
                    agentState = remoteAgentService.disable(agentId);              
                    return new UpgradePermissionModel(PermissionEnum.YES_CHILD,"you may upgrade once idle",agentState,config.getTargetVersion(),task.getID());                    
                }catch(AlreadyInProgressException aip){
                    return new UpgradePermissionModel(PermissionEnum.UH_OH,PermissionEnum.UH_OH.getMessage(),agentState,config.getTargetVersion(),aip.getTaskId());                    
                }
            }else{
                log.info("asking agent {} to wait since the max # is already reached",agentId);
                
                return new UpgradePermissionModel(PermissionEnum.WAIT_FOR_SIBLINGS,currentActivity.size() + " agents alreaddy updating, please check back soon.",agentState);
            }
        }else{
            log.info("Locked in stable state, no updates allowed. SOrry agent {}",agentId);
            
            return new UpgradePermissionModel(PermissionEnum.NO_CHILD,config.getStance().getDescription(),agentState);
        }
    }



    @Override
    public UpgradePermissionModel markComplete( int taskId) {
        AgentInProgress taskDetails = agentInProgressService.markTaskComplete(taskId);
        AgentStateModel agentState = remoteAgentService.getStateFor(taskDetails.getAgentId());
        if(taskDetails.isAgentActive()){
            agentState = remoteAgentService.enable(taskDetails.getAgentId());
            log.warn("Agent was active before maintenance, so we will mark it re-enabled.");
        }else{
            log.warn("Agent was disabled before requesting maintenance, so it will not be activated now");
        }
        return new UpgradePermissionModel(PermissionEnum.NO_CHILD,"Thanks for updating, letting the others catch up.",agentState);
        
    }

    @Override
    public VersionModel pollVersion() {
        AdminConfigurationResource config = configurationService.getActiveConfig();
        return new VersionModel(config.getTargetVersion());
    }

    @Override
    public void abortTask(int taskId) {
        AgentInProgress taskDetails = agentInProgressService.markTaskComplete(taskId);
        agentInProgressService.markTaskComplete(taskId);
    }

    @Override
    public List<TaskModel> listAllOpenTasks(){
        List<TaskModel> tasks = new ArrayList<TaskModel>();
        List<AgentInProgress> currentActivity = agentInProgressService.existingActivityForAllAgents();
        for( AgentInProgress ao : currentActivity){
            tasks.add(taskModelFromAo(ao));
        }
        return tasks;
    }
    @Override
    public TaskModel findOpenTask(long agentId) {
        List<AgentInProgress> currentActivity = agentInProgressService.existingActivityFor(agentId);
        if(currentActivity.size() > 0){
            return taskModelFromAo(currentActivity.get(0));
        }else{
            return new TaskModel();
        }

    }

    private TaskModel taskModelFromAo(AgentInProgress ao){
        TaskModel task = new TaskModel()
                .setActivity(ao.getActivity())
                .setAgentId(ao.getAgentId())
                .setAgentName(ao.getName())
                .setStartDate(ao.getSTartDate())
                .setEndDate(ao.getEndtDate())
                .setUuid(ao.getUuid())
                .setId(ao.getID());
        return task;
    }

}
