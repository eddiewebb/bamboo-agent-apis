package com.edwardawebb.bamboo.agentapis.services;

import java.util.List;
import java.util.UUID;

import com.edwardawebb.bamboo.agentapis.enums.AgentActivity;
import com.edwardawebb.bamboo.agentapis.rest.admin.TaskModel;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.UpgradePermissionModel;
import com.edwardawebb.bamboo.agentapis.rest.agents.maintenance.VersionModel;

public interface ChaperoneService {

    UpgradePermissionModel markInProgressIfAllowed(AgentActivity upgrade, long agentId,UUID uuid);

    UpgradePermissionModel markComplete( int taskId);

    VersionModel pollVersion();

    void abortTask(int taskId);

    List<TaskModel> listAllOpenTasks();

    TaskModel findOpenTask(long agentId);
}
