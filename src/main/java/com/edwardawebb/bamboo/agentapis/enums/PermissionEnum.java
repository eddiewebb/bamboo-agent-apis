package com.edwardawebb.bamboo.agentapis.enums;

import com.fasterxml.jackson.annotation.JsonFormat;



@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum PermissionEnum {

    YES_CHILD(0,"Yes, you may upgrade as you wish.",true),
    NO_CHILD(1,"No Child, we must be stable now.",false),
    WAIT_FOR_SIBLINGS(2,"Please check back shortly, too many other agents are already busy",false),
    UH_OH(3,"Records indicate this agents is already upgrading, but has since asked for permission! Please mark the provided task complete before requesting additonal work.",false);
    
    
    private int code;
    private String message;
    private boolean isYes;
    
    PermissionEnum(int code, String message,boolean isYes){
        this.code = code;
        this.message = message;
        this.isYes = isYes;
    }

    public boolean isYes() {
        return this.isYes;
    }
    
    public String getMessage(){
        return message;
    }
}
