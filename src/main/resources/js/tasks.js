

console.log("DEBUG: Tasks JS Loading");

(function ($) { // this closure helps us keep our variables to ourselves.
// This pattern is known as an "iife" - immediately invoked function expression


    // form the URL
    var url = AJS.contextPath() + "/rest/agent-config/1.0/tasks";


    function loadTasks() {
		console.log("DEBUG: Loading Tasks from " + url);
    	  // request the config information from the server
        AJS.$.ajax({
            url: url,
            dataType: "json",

		    success: function(data) {
	            console.log("Tasks returned success,")
	        	console.log(data);
	        	var tasks = Bamboo.Templates.AgentAPIRules.taskRows({tasks:data,baseUrl:AJS.contextPath()});
	 	         AJS.$("#taskRows").html(tasks);
	        },
	        error:function(request,status,error) {
				console.log("Tasks returned ERROR: " + status);
				console.log(error);
			}
	    });
    }

	function deleteTask(event) {
		console.log(event);
		var target = event.target || e.srcElement;
		var taskid = AJS.$(target).data("task-id");
		console.log("deleting task with UUID " + taskid);
		AJS.$.ajax({
			    url: url + "/" + taskid,
			    type: "DELETE",
			    contentType: "application/json",
			    processData: false,
			    success: function(data) {
			         AJS.messages.success({
			            title: "Deleted!",
			            body: "Task '" + taskid + "' was successfully deleted."
			         });
			         loadTasks();
			     }
		 });
	}


    // wait for the DOM (i.e., document "skeleton") to load. This likely isn't
	// necessary for the current case,
    // but may be helpful for AJAX that provides secondary content.
    $(document).ready(function() {
    	//bind our buttons, etc
    	AJS.$(document).on('click', "a.deleteTask", function(e) {
    	    e.preventDefault();
    		deleteTask(e);
    		return false;
    	});
    	// load data
        loadTasks();
    });

})(AJS.$ || jQuery);

